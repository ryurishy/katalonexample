import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String[] usernameArray = ["John Mayer", "Jaka gledek", "Lalala yeyeye", "Adam", "Jaka", "Nadia", "John Snow", "Rama", "Nada", "John Doe"]
String[] passwordArray = ["123", "1234", "123123", "asdad", "anda", "9283h", "manda", "delapan", "sembilan", "ThisIsNotAPassword"]


WebUI.openBrowser(null)

WebUI.navigateToUrl(G_SiteURL)

WebUI.click(findTestObject('Page_CURA Healthcare Service/a_Make Appointment'))

for (int r = 0; r < usernameArray.size() ; r++){
	
	WebUI.setText(findTestObject('Page_CURA Healthcare Service/input_Username_username'), usernameArray[r])
	
	WebUI.setText(findTestObject('Page_CURA Healthcare Service/input_Password_password'), passwordArray[r])
	
	WebUI.click(findTestObject('Page_CURA Healthcare Service/button_login'))
}

WebUI.closeBrowser()

